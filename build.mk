#!/usr/bin/make -f

# Buildscript for current GCC cross toolchain 
# -------------------------------------------
#
# Sources for binutils, gmp, mpfr, mpc and gcc must be unpacked and 
# patched in "src"
#
# GCC needs softfloat patch applied, other patches may be required. 
# Patches can be obtained from crosstool or crosstool-ng repositories, 
# some GCC patches are also included in "patches".
#
# GPH SDK needs to be unpacked in "GPH_SDK"
#
# Toolchain will be installed in "{wiz|caanoo}-toolchain-$GCC_VERSION",
# and can be copied to a different folder afterwards.
#
# Create file "config.mk", for example: 
#
# --
# BINUTILS_VERSION = 2.16.1
# GCC_VERSION = 4.6.4
# GMP_VERSION = 5.0.5
# MPFR_VERSION = 3.1.2
# MPC_VERSION = 0.8.2
# --
#
# Run as ./build.mk {wiz|caanoo}

# ------------------------
BINUTILS_VERSION = BINUTILS_VERSION_NOT_SET
GCC_VERSION = GCC_VERSION_NOT_SET
GMP_VERSION = GMP_VERSION_NOT_SET
MPFR_VERSION = MPFR_VERSION_NOT_SET
MPC_VERSION = MPC_VERSION_NOT_SET

include config.mk

# ------------------------
TARGET_WIZ ?= arm-wiz-linux-gnu
TARGET_CAANOO ?= arm-caanoo-linux-gnueabi
GPH_SDK ?= $(CURDIR)/GPH_SDK

# ------------------------
PATH += :$(PREFIX)/bin
export PATH

COMMON_CONFARGS = \
	--disable-nls \
	--prefix="$(PREFIX)" \
	--target="$(TARGET)" \
	--with-sysroot

GCC_CONFARGS = \
	--disable-libgomp \
	--disable-libmudflap \
	--disable-libssp \
	--disable-multilib \
	--disable-tls \
	--enable-__cxa_atexit \
	--enable-c99 \
	--enable-cxx-flags="-mcpu=arm926ej-s -msoft-float -mfpu=vfp $(ABI_CXXFLAGS)" \
	--enable-long-long \
	--enable-symvers=gnu \
	--enable-threads=posix \
	--with-cpu=arm926ej-s \
	--with-float=soft \
	--with-fpu=vfp \
	--with-gmp="$(CURDIR)/static" \
	--with-mpc="$(CURDIR)/static" \
	--with-mpfr="$(CURDIR)/static" \
	$(ABI_CONFARGS)

STAMPS_BUILD = stamp-binutils stamp-gmp stamp-mpfr stamp-mpc stamp-gcc 
STAMPS_ALL = $(STAMPS_BUILD) stamp-sysroot-wiz stamp-sysroot-caanoo

PACKAGE = PACKAGE_NOT_SET
VERSION = VERSION_NOT_SET
CONFARGS = 
TARGET = TARGET_NOT_SET
ABI_CONFARGS =
ABI_CXXFLAGS =

all: 
	echo "Invalid target"
	false

wiz: PREFIX = $(CURDIR)/wiz-toolchain-$(GCC_VERSION)
wiz: TARGET = $(TARGET_WIZ)
wiz: ABI_CONFARGS = --enable-obsolete
wiz: stamp-sysroot-wiz stamp-gcc

caanoo: PREFIX = $(CURDIR)/caanoo-toolchain-$(GCC_VERSION)
caanoo: TARGET = $(TARGET_CAANOO)
caanoo: ABI_CONFARGS = --with-abi=aapcs-linux
caanoo: ABI_CXXFLAGS = -mabi=aapcs-linux
caanoo: stamp-sysroot-caanoo stamp-gcc

stamp-sysroot-wiz:
	mkdir -p "$(PREFIX)/$(TARGET_WIZ)/sys-root/usr"
	cp -r "$(GPH_SDK)/tools/gcc-4.0.2-glibc-2.3.6/arm-linux/arm-linux/include" "$(PREFIX)/$(TARGET_WIZ)/sys-root/usr/"
	cp -r "$(GPH_SDK)/tools/gcc-4.0.2-glibc-2.3.6/arm-linux/arm-linux/lib" "$(PREFIX)/$(TARGET_WIZ)/sys-root/"
	ln -s ../lib "$(PREFIX)/$(TARGET_WIZ)/sys-root/usr/lib"
	touch $@

stamp-sysroot-caanoo:
	mkdir -p $(PREFIX)/$(TARGET_CAANOO)
	cp -r "$(GPH_SDK)/tools/gcc-4.2.4-glibc-2.7-eabi/arm-gph-linux-gnueabi/sys-root" "$(PREFIX)/$(TARGET_CAANOO)/"
	touch $@

stamp-binutils: PACKAGE = binutils
stamp-binutils: VERSION = $(BINUTILS_VERSION)

stamp-gmp: PACKAGE = gmp
stamp-gmp: VERSION = $(GMP_VERSION)
stamp-gmp: COMMON_CONFARGS = --prefix="$(CURDIR)/static" --disable-shared

stamp-mpfr: stamp-gmp
stamp-mpfr: PACKAGE = mpfr
stamp-mpfr: VERSION = $(MPFR_VERSION)
stamp-mpfr: COMMON_CONFARGS = --prefix="$(CURDIR)/static" --disable-shared --with-gmp="$(CURDIR)/static" 

stamp-mpc: stamp-gmp stamp-mpfr
stamp-mpc: PACKAGE = mpc
stamp-mpc: VERSION = $(MPC_VERSION)
stamp-mpc: COMMON_CONFARGS = --prefix="$(CURDIR)/static" --disable-shared --with-gmp="$(CURDIR)/static" --with-mpfr="$(CURDIR)/static"

stamp-gcc: stamp-binutils stamp-gmp stamp-mpfr stamp-mpc
stamp-gcc: PACKAGE = gcc
stamp-gcc: VERSION = $(GCC_VERSION)
stamp-gcc: CONFARGS = \
	$(GCC_CONFARGS) \
	--enable-languages=c,c++

$(STAMPS_BUILD): BUILDDIR = $(CURDIR)/build/$(PACKAGE)-$(VERSION)
$(STAMPS_BUILD): SRCDIR = $(CURDIR)/src/$(PACKAGE)-$(VERSION)
$(STAMPS_BUILD): 
	rm -fr "$(BUILDDIR)"
	mkdir -p "$(BUILDDIR)"
	cd "$(BUILDDIR)" && "$(SRCDIR)/configure" \
		$(COMMON_CONFARGS) \
		$(CONFARGS)

	make -C "$(BUILDDIR)" -j4
	make -C "$(BUILDDIR)" install
	touch $@

clean:
	$(RM) $(STAMPS_ALL)

.PHONY: all wiz caanoo clean
